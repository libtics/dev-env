#!/bin/bash

pdfs="N"
nvm_version="v0.39.0"
ruby_version="2.7.0"
node_version="v16.3.0"
antora_version="3.0.0"

BUILD_DIR=$HOME/denv-build
mkdir -p $BUILD_DIR

function plog() {
	msg="Provisioning: $1"
	echo "$msg"
}

plog "$(date)"
plog "Setting up for libtics development in $BUILD_DIR"

if [ "$pdfs" == "Y" ]; then
	plog "ruby install/update starting"

	rbenv install -v "$ruby_version" 2>&1
	rbenv global "$ruby_version" 2>&1
	export PATH="$HOME/.rbenv/bin:$PATH"
	eval "$(rbenv init -)"
	if [ "$(grep 'rbenv init' $HOME/.bashrc)" == "" ]; then
		cat >> $HOME/.bashrc <<-'__EOF'
			# Added by libtics provisioning
			set -o vi
			export PATH="$HOME/.rbenv/bin:$PATH"
			eval "$(rbenv init -)"
		__EOF
	fi
	plog "ruby install/update finished"

	plog "ruby bundler install/update starting"
	cd $BUILD_DIR
	if [ ! -f Gemfile ]; then
		cp /tmp/Gemfile . || exit 1
	fi
	bundler --verbose 2>&1
	cd - > /dev/null 2>&1
	plog "ruby bundler install/update finished"
fi

if [ "$(grep 'Added by libtics provisioning' $HOME/.bashrc)" == "" ]; then
	cat >> $HOME/.bashrc <<-'__EOF'
		# Added by libtics provisioning
		set -o vi
		alias ls="ls --color=never"
		export PS1='\u@\h \w $ '
		export PATH=$PATH:$HOME/bin
	__EOF
fi

# Create a utility script, ltb, that can be invoked to do
# a libtics build either from inside or outside the container/vm
# And ctz to set a timezone interactively
rm -rf $HOME/bin
mkdir -p $HOME/bin
cat >> $HOME/bin/ltb <<-'__EOF'
	#!/bin/bash
	export LOG_LEVEL=${LL:=warn}
	export NVM_DIR="$HOME/.nvm"
	[ -s "$NVM_DIR/nvm.sh" ] && \. "$NVM_DIR/nvm.sh"  # This loads nvm
	cd $HOME/libtics/site-builder && make
__EOF
chmod u+x $HOME/bin/ltb
cat >> $HOME/bin/ctz <<-'__EOF'
	#!/bin/bash
	/usr/sbin/dpkg-reconfigure tzdata
__EOF
chmod u+x $HOME/bin/ctz

curl -s -o- \
	https://raw.githubusercontent.com/nvm-sh/nvm/"$nvm_version"/install.sh \
	| bash 2>&1

plog "nvm install/update starting"

export NVM_DIR="$HOME/.nvm"
[ -s "$NVM_DIR/nvm.sh" ] && \. "$NVM_DIR/nvm.sh"  # This loads nvm

plog "nvm install/update finished"

plog "node install/update starting"

nvm install --no-progress "$node_version" 2>&1

plog "node install/update finished"

plog "npm install/update starting"

npm -g update >> /dev/null 2>&1
npm i -g gulp-cli @antora/cli@"$antora_version" \
	@antora/site-generator@"$antora_version" \
	asciidoctor-kroki \
	2>&1

plog "npm install/update finished"

# Seems to be necessary so it can find asciidoctor for the message below
eval "$(rbenv init -)"

plog "node is $(which node)"
plog "npm is $(which npm)"
plog "antora is $(which antora) - version $(antora -v)"

if [ "$pdfs" == "Y" ]; then
	plog "ruby is $(which ruby) - version is $(ruby -v)"
	ad_ver=$(asciidoctor -v | grep Ascii | cut -f1 -d'[')
	plog "asciidoctor is $(which asciidoctor) - version $ad_ver"
fi

plog "complete"
